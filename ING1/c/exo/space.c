#include <stdio.h>
#include <string.h>

void tranform(char *str)
{
  for (size_t i = 0; i < strlen(str); i++)
  {
    if (str[i] == '\t')
      str[i] = ' ';
  }
}

void space(char *str)
{
  int t = 0;
  for (size_t i = 0; i < strlen(str) - 1; i++)
  {
    if (t == 1)
    {
      if (str[i] == ' ' || str[i] == '\t')
      {
        str[i] = '\a';
      }
      else
        t = 0;
    }
    else
    {
      if (str[i] == ' ' || str[i] == '\t')
        t = 1;
    }
  }
}

int main(void)
{
  char str1[] = "Ceci est    un test \t !";
  char str2[] = "tu \tes un \tdebile   !";
  char str3[] = "Tu as    \t     loupé ton rattrapage !";

  space(str1);
  printf("%s\n", str1);

  space(str2);
  printf("%s\n", str2);

  space(str3);
  printf("%s\n", str3);

  return 1;
}
