#define _POSIX_SOURCE

#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[])
{
  if (argc > 1 || argc == 0)
    return 2;
  FILE *f = fopen(argv[1], 'r');
  if (!f)
    return 1;
  char *buf;
  char *cpy;
  while((cpy = fgets(buf, 512, f)) != EOF)
  {
    for (int i = strlen(buf) - 1; i > 0; i--)
      printf("%c", buf[i]);
    printf("\n");
  }
  return 0;
}

