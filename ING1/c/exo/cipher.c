#include <stdio.h>
#include <string.h>

void cipher(const char *key, const char *msg, char *res)
{
  char k;
  char m;
  int j = 0;
  for (size_t i = 0; i < strlen(msg); i++)
  {
    if (msg[i] == ' ' || msg[i] == '\t')
      j++;
    if (key[(i - j) % strlen(key)] > 96 && key[(i - j) % strlen(key)] < 123)
      k = key[(i - j) % strlen(key)] - 32 - 65;
    else if (key[(i - j) % strlen(key)] > 64 && key[(i - j) % strlen(key)] < 91)
      k = key[(i - j) % strlen(key)] - 65;
    else
      k = msg[i];
    if (msg[i] > 96 && msg[i] < 123)
      m = msg[i] - 32 - 65;
    else if (msg[i] > 64 && msg[i] < 91)
      m = msg[i] - 65;
    else
      m = msg[i];
    if (k != msg[i] && m != msg[i])
      res[i] = (k + m) % 26 + 65;
    else
      res[i] = msg[i];
  }
}

void decipher(const char *key, const char *msg, char *res)
{
  char k;
  char m;
  int j = 0;
  for (size_t i = 0; i < strlen(msg); i++)
  {
    if (msg[i] == ' ' || msg[i] == '\t')
      j++;
    if (key[(i - j) % strlen(key)] > 96 && key[(i - j) % strlen(key)] < 123)
      k = key[(i - j) % strlen(key)] - 32 - 65;
    else if (key[(i - j) % strlen(key)] > 64 && key[(i - j) % strlen(key)] < 91)
      k = key[(i - j) % strlen(key)] - 65;
    else
      k = msg[i];
    if (msg[i] > 96 && msg[i] < 123)
      m = msg[i] - 32 - 65;
    else if (msg[i] > 64 && msg[i] < 91)
      m = msg[i] - 65;
    else
      m = msg[i];
    if (k != msg[i] && m != msg[i])
      res[i] = (m - k + 26) % 26 + 65;
    else
      res[i] = msg[i];
  }
}

int main(void)
{
  char str[] = "HeLlO WorLd";

  cipher("TotO", str, str);
  printf("%s\n", str);

  decipher("TotO", str, str);
  printf("%s\n", str);

  return 0;
}
