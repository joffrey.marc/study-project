#include <iostream>
#include <memory>
#include <optional>

#include "int-container.hh"

MyIntContainer::MyIntContainer(size_t size): size_(size)
{
  arr_ = new int[size_];
  len_ = 0;
}

MyIntContainer::~MyIntContainer()
{
  delete[] arr_;
}

void MyIntContainer::print() const
{
  size_t i = 0;
  for (; i < (len_ - 1); i++)
    std::cout << arr_[i] << " | ";
  std::cout << arr_[i] << std::endl;
}

size_t MyIntContainer::get_len() const
{
  return len_;
}

bool MyIntContainer::add(int elem)
{
  if ((len_ + 1) > size_)
    return false;
  else
  {
    arr_[len_] = elem;
    len_++;
    return true;
  }
}

std::optional<int> MyIntContainer::pop()
{
  if (len_ == 0)
    return std::nullopt;
  else
  {
    len_--;
    return std::optional<int> {arr_[len_]};
  }
}

std::optional<int> MyIntContainer::get(size_t position) const
{
  if ((position > (len_ - 1)))
    return std::nullopt;
  else
    return std::optional<int> {arr_[position]};
}

std::optional<size_t> MyIntContainer::find(int elem) const
{
  for (size_t i = 0; i < (len_ - 1); i++)
  {
    if (arr_[i] == elem)
      return std::optional<size_t> {i};
  }
  return std::nullopt;
}

void MyIntContainer::sort()
{
  for (size_t i = 0; i < (len_ - 1); i++)
  {
    int x = arr_[i];
    auto j = i;
    while ((j > 0) && arr_[j - 1] > x)
    {
      arr_[j] = arr_[j - 1];
      j--;
    }
    arr_[j] = x;
  }
}

bool MyIntContainer::is_sorted() const
{
  for (size_t i = 0; i < (len_ - 2); i++)
  {
    if (arr_[i] > arr_[i + 1])
      return false;
  }
  return true;
}

int main()
{
  MyIntContainer c = MyIntContainer(10);
  for (int i = 0; i < 11; ++i)
    c.add(i);
  c.print();
  if (auto el = c.pop())
    std::cout << "element poped: " << *el << '\n';
  else
    std::cout << "Trying to pop an empty array" << '\n';
  std::cout << std::endl;
  for (int i = 0; i < 10; ++i)
  {
    if (auto el = c.get(i))
      std::cout << "got element: " << *el << '\n';
    else
      std::cout << "Trying to access out of bound index: " << i << '\n';
  }
  if (auto pos = c.find(9))
    std::cout << "Position of element `9`: " << *pos << '\n';
  else
    std::cout << "Failed to find number 9" << '\n';
  return 0;
}
