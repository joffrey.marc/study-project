#ifndef INT_CONTAINER_HH
#define INT_CONTAINER_HH

#include <iostream>
#include <memory>
#include <optional>

class MyIntContainer
{
  public :
    MyIntContainer(size_t size);
    ~MyIntContainer();
    void print() const;
    size_t get_len() const;
    bool add(int elem);
    std::optional<int> pop();
    std::optional<int> get(size_t position) const;
    std::optional<size_t> find(int elem) const;
    void sort();
    bool is_sorted() const;

  private :
    size_t size_;
    size_t position_;
    int* arr_;
    size_t len_;
};

#endif
