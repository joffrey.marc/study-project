#ifndef ROBOT_HH
#define ROBOT_HH

#include <iostream>
#include <memory>
#include <string>

class Robot
{
public : 
  Robot(int id, const std::string& country);
  void change_country(const std::string& country);
  const std::string& get_country() const;
  bool is_alive() const;
  virtual void take_damage(size_t attack) = 0;
  virtual void print() const = 0;
  virtual void fight(Robot& enemy) const = 0;

protected :
  int id_;
  int health_;
  std::string& country_;
  size_t attack_;
  Robot& enemy_;
};

#endif
