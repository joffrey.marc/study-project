#include <iostream>
#include <memory>
#include <string>
#include "armored-robot.hh"

ArmoredRobot::ArmoredRobot(int id, const std::string& country,
                           size_t armor, size_t attack): armor_(armor), id_(id),
                           country_(country), attack_(attack)
{}

void  ArmoredRobot::take_damage(size_t attack)
{
  if (attack < armor_)
    armor_ -= attack;
  else
  {
     attack -= armor_;
     armor_ = 0;
     health_ -= attack;
  }
}

void ArmoredRobot::print() const
{
  std::cout << "Reporting for duty! I'm armored robot number " << id_ 
  << ". I am currently in " << country_ << ". I have " << armor_ 
  << " armor and " << health_ << "hp left !" << std::endl;
}

void ArmoredRobot::fight(Robot& enemy) const
{
  if (enemy.get_country() == country_)
    enemy.take_damage(attack_);
}
