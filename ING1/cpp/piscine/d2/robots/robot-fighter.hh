#ifndef ROBOT_FIGHTER_HH
#define ROBOT_FIGHTER_HH

#include <iostream>
#include <memory>
#include <string>

class RobotFighter : public Robot
{
public :
  RobotFighter(int id, const std::string& country, size_t attack);
  void take_damage(size_t attack) override;
  void print() const override;
  void fight(Robot& enemy) const override;

protected :
  size_t attack_;
};

#endif
