#include <iostream>
#include <string>
#include <memory>
#include "robot.hh"

Robot::Robot(int id, const std::string& country): id_(id), country_(country)
{
  health_ = 100;
}

void Robot::change_country(const std::string& country)
{
  country_(country);
}

const std::string& Robot::get_country() const
{
  return country_;
}

bool Robot::is_alive() const
{
  if (health_ > 0)
    return true;
  else
    return false;
}
