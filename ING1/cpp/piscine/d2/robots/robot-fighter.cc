#include <iostream>
#include <memory>
#include <string>
#include "robot-fighter.hh"

RobotFighter::RobotFighter(int id, const std::string& country, size_t attack):
                           id_(id), country_(country), attack_(attack)
{}

RobotFighter::take_damage(size_t attack)
{
  health_ -= attack;
}

void RobotFighter::print() const
{
  std::cout << "Reporting for duty! I'm robot fighter number " << id_ 
  << ". I am currently in " << country_ << ". I still have " << health_
  << "hp left !" << std::endl;
}

void RobotFighter::fight(Robot& enemy) const
{
  if (enemy.get_country() == country_)
    enemy.take_damage(attack_);
}
