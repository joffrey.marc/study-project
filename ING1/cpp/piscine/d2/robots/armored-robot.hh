#ifndef ARMORED_ROBOT_HH
#define ARMORED_ROBOT_HH

#include <iostream>
#include <memory>
#include <string>

class ArmoredRobot: public Robot
{
public :
  ArmoredRobot(int id, const std::string& country, size_t armor, size_t attack);
  void take_damage(size_t attack) override;
  void print() const override;
  void fight(Robot& enemy) const override;

protected :
  size_t armor_;
};

#endif
