#include <iostream>
#include <string>
#include <fstream>

#include "replace.hh"

void replace(const std::string& input_filename,
    const std::string& output_filename,
    const std::string& src_token,
    const std::string& dst_token)
{
  std::ifstream file_in;
  std::ofstream file_out;
  std::string line;
  
  size_t l_len = 0;
  auto s_len = src_token.size();
  
  file_in.open(input_filename);
  if (!file_in.is_open())
    std::cerr << "Cannot open input file" << std::endl;
  
  file_out.open(output_filename);
  if (!file_out.is_open())
    std::cerr << "Cannot write output file" << std::endl;
  
  while (getline(file_in, line))
  {
    size_t pos = line.find(src_token, 0);
    l_len = line.size();

    while (pos < l_len)
    {
      line.replace(pos, s_len, dst_token);
      pos = line.find(src_token, pos);
    }

    file_out << line << std::endl;
  }

  file_in.close();
  file_out.close();
}
