#pragma once

#include <iostream>

template<typename fct>
class Cleanup
{
public :
  Cleanup(fct lambda);
  ~Cleanup()
private:
  fct lambda_;
};

