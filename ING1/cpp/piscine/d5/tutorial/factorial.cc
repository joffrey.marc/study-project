#include <iostream>

constexpr unsigned long long factorial(unsigned int n)
{
  if ( n == 0)
    return 1;
  else 
    return n * factorial(n - 1);
}

int main()
{
  constexpr auto facto = factorial(65);
  std::cout << facto << "\n";
  return 0;
}
