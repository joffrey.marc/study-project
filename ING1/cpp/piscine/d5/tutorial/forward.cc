#include <iostream>

int main()
{
  auto forward = [](auto a)
  {
    auto c = [a] (auto b)
    {
      return a * b;
    }
    return c;
  }
}
