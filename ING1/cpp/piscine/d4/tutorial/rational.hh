#pragma once

#include <iostream>

class Rational
{
  public : 
    Rational(int num, int den);
    int numerator() const;
    int denominator() const;
    Rational operator*(Rational k);
    Rational operator+(Rational k);
    void operator*=(Rational k);

friend std::ostream& operator<<(std::ostream& ostr, const Rational& k);

  private : 
    int num_;
    int den_;
};
