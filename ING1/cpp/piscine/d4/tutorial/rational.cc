#include "rational.hh"

Rational::Rational(int num, int den): num_(num), den_(den)
{}

std::ostream& operator<<(std::ostream& ostr, const Rational& k)
{
  ostr << "Num : " << k.num_ << "Den : " << k.den_ << std::endl;
  return ostr;
}

int Rational::numerator() const
{
  return num_;
}

int Rational::denominator() const
{
  return den_;
}

Rational Rational::operator*(Rational k)
{
  return Rational(k.num_ * num_, k.den_ * den_);
}

Rational Rational::operator+(Rational k)
{
  if (k.den_ == den_) 
    return Rational(k.num_ * num_, den_);
  else
    return Rational(k.num_ * den_ + num_ * k.den_, k.den_ * den_); 
}


void Rational::operator*=(Rational k)
{
  num_ *= k.num_;
  den_ *= k.den_;
}

int main()
{
  Rational i = Rational(4 , 3);
  Rational j = Rational(3 , 4);
  std::cout << i + j << "\n";
  std::cout << i * j << "\n";
  i *= j;
  std::cout << i << "\n";
}
