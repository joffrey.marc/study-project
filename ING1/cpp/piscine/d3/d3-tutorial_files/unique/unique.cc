#include <iostream>
#include <vector>
#include "unique.hxx"

int main()
{
    std::vector<int> v1{1, 2, 1};
    std::vector<int> v2{1, 2, 3};
    std::vector<char> v3{'a', 'b', 'c', 'd', 'e'};
    std::vector<char> v4{'a', 'b', 'b', 'd', 'e'};

    std::cout << std::boolalpha;
    std::cout << "v1: " << unique(v1) << '\n';
    std::cout << "v2: " << unique(v2) << '\n';
    std::cout << "v3: " << unique(v3) << '\n';
    std::cout << "v4: " << unique(v4) << '\n';
}
