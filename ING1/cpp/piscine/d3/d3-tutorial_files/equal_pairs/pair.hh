#pragma once

#include "pair.hxx"

template <typename T1, typename T2>
class Pair
{
public:
  Pair(T1 v1, T2 v2)
    : v1_(v1)
    , v2_(v2)
  {}

  T1& first() { return v1_; }
  const T1& first() const { return v1_; }

  T2& second() { return v2_; }
  const T2& second() const { return v2_; }

  bool operator==(const Pair& P) const
  {
    return v1_ == P.v1_ && v2_ == P.v2_;
  }

  bool operator!=(const Pair& P) const
  {
    return v1_ != P.v1_ || v2_ != P.v2_;
  }

friend bool operator==(const Pair& p1, const Pair& p2);
friend bool operator!=(const Pair& p1, const Pair& p2);

private:
  T1 v1_;
  T2 v2_;
};
