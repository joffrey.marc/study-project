#pragma once

#include "pair.hh"

template <typename T1, typename T2>
bool operator==(const Pair<T1,T2>& p1, Pair<T1,T2>& p2) const
{
  return v1_ == p1.v1_ && v2_ == p2.v2_;
}

bool operator!=(const Pair<T1,T2>& p1, Pair<T1,T2>& p2) const
{
   return v1_ != p1.v1_ || v2_ != p2.v2_;
}

