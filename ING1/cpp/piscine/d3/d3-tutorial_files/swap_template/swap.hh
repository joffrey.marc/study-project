#include <iostream>
#include "swap.hxx"

#pragma once
template <typename T1>
void swap(T1& p1, T1& p2);
