#pragma once

#include <iostream>
#include "swap.hh"

template<typename T1>
void swap(T1& p1, T1& p2)
{
  auto tmp = p1;
  p1 = p2;
  p2 = tmp;
}

